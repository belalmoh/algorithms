class MyCircularQueue {
  constructor(length){
    this._Front = -1;
    this._Rear = -1;
    this._Length = length;
    this._queue = new Array(length).fill(undefined);
  }

  get Front(){
    return this._Front === -1 ? -1 : this._queue[this._Front]
  }

  get Rear(){
    return this._Rear === -1 ? -1 : this._queue[this._Rear]
  }

  enQueue(value){
    if(this.isEmpty()){

      if(this._Front === -1 && this._Rear === -1){
        this._Front = this._Rear = this.getFirstEmpty()
      } else if(this._Rear !== -1) {
        this._Rear = this.getFirstEmpty();
      }

      this._queue[this._Rear] = value
      return true;
    }
    return false
  }

  deQueue(){
    if(!this.isPristine()){
      
      if(this._Front !== this._Rear && this._Front >= 0){
        this._queue[this._Front] = undefined;
        this._Front += 1;
        
      } else if(this._Front === this._Rear && (this._Front === 0 || this._Front === this._Length -1)){
        this._queue[this._Front] = undefined;
        this._Front = this._Rear = -1
      }

      return true;
    }
    return false
  }

  isEmpty(){
    let hasUndefined = this._queue.findIndex(elem => elem == null)
    return hasUndefined !== -1
  }

  isFull(){
    return !this.isEmpty()
  }

  getFirstEmpty(){
    return this._queue.findIndex(elem => elem == null) 
  }

  isPristine(){
    return this._Rear === this._Front && this._Front == -1
  }
}

module.exports = MyCircularQueue